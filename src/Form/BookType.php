<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Author;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('author', EntityType::class, [
            // looks for choices from this entity
            'class' => Author::class,
            'choice_label' => 'name',

            // used to render a select box, check boxes or radios
//             'multiple' => true,
//             'expanded' => true,
            ])
            ->add('category', EntityType::class, [
                // looks for choices from this entity
                'class' => Category::class,
                'choice_label' => 'name',

                // used to render a select box, check boxes or radios
//             'multiple' => true,
//             'expanded' => true,
            ])
            ->add('title')
            ->add('description')
            ->add('published_date', DateType::class, [
                'widget' => 'single_text',
                'data'   => new \DateTime(),
                'attr'   => ['max' => ( new \DateTime() )->format('Y-m-d')]
            ])
            ->add('price');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
