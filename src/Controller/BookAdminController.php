<?php

namespace App\Controller;


use App\Entity\Book;
use App\Form\BookType;
use App\Helper\Constants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

// Include paginator interface
use Knp\Component\Pager\PaginatorInterface;

/**
 * Class BookAdminController
 * @package App\Controller
 * @IsGranted("ROLE_ADMIN")
 */
class BookAdminController extends AbstractController
{
    /**
     * @Route("/admin/book", name="admin_book")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $bookRepository = $this->getDoctrine()->getRepository(Book::class);
        $allBooks = $bookRepository->findAll();

        $books = $paginator->paginate(
            $allBooks,
            $request->query->getInt('page', 1),
            Constants::PER_PAGE
        );

        return $this->render('book/index.html.twig', [
            'books' => $books
        ]);
    }

    /**
     * @Route("/admin/book/add", name="admin_book_add")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request)
    {
        $book = new Book();

        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $book = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            return $this->redirectToRoute('admin_book');
        }

        return $this->render('book/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
