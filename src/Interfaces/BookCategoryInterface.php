<?php

namespace App\Interfaces;


interface BookCategory
{
    public function getDiscount();
}