<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BookFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
//        $categoryChildren = new Category();
//        $categoryChildren->setName('Children');
//        $manager->persist($categoryChildren);
//
//        $categoryFiction = new Category();
//        $categoryChildren->setName('Fiction');
//        $manager->persist($categoryFiction);
//
//        for ($i=0; $i<50; $i++) {
//            $book = new Book();
//            $book->setTitle("Children Book " . $i);
//            $book->setCategory($categoryChildren);
//
//            $author = new Author('Author' . $i);
//            $book->setAuthor($author);
//            $manager->persist($book);
//        }

        $manager->flush();
    }
}
