<?php


namespace App\Helper;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ErrorResponse
 * @package App\Helper
 */
class ErrorResponse extends JsonResponse
{
    /**
     * Send Error response.
     * @param string $message
     * @param int $code
     * @return JsonResponse|null
     */
    public function __invoke($message, $code = Response::HTTP_BAD_REQUEST)
    {
        return new JsonResponse(
            json_encode($message),
            $code
        );
    }
}
