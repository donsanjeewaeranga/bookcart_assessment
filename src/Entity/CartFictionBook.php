<?php

namespace App\Entity;


use App\Interfaces\BookCategory;

class CartFictionBook implements BookCategory
{
    private $items;
    private $qty;
    private $categoryTotal;

    public function __construct($oldCartFictionBook)
    {
        if ($oldCartFictionBook) {
            $this->items = $oldCartFictionBook->items;
            $this->qty = $oldCartFictionBook->qty;
            $this->categoryTotal = $oldCartFictionBook->categoryTotal;
        }
    }

    /**
     * @param $item
     * @param $removeQty
     */
    public function add($item, $removeQty)
    {
        $id = $item->getId();
        $storeItem = ['qty' => 0, 'price' => $item->getPrice(), 'item' => $item];

        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storeItem = $this->items[$id];
            }
        }
        $storeItem['qty'] += $removeQty;
        $storeItem['price'] = $item->getPrice() * $storeItem['qty'];
        $this->items[$id] = $storeItem;
        $this->qty += $removeQty;
        $this->categoryTotal += ($item->getPrice() * $removeQty);
    }

    /**
     * @param $item
     * @param $removeQty
     * @return bool
     */
    public function remove($item, $removeQty)
    {
        $id = $item->getId();

        if (!$this->items || !array_key_exists($id, $this->items)) {
            return false;
        }
        $storeItem = $this->items[$id];

        $storeItem['qty'] -= $removeQty;

        if ($storeItem['qty'] == 0) {
            unset($this->items[$id]);
        } else {
            $storeItem['price'] = $item->getPrice() * $storeItem['qty'];
            $this->items[$id] = $storeItem;
        }

        $this->qty -= $removeQty;
        $this->categoryTotal -= ($item->getPrice() * $removeQty);
    }

    public function getDiscount()
    {
        // TODO: Implement getDiscount() method if needed in the category level.
    }

    public function getQuantity()
    {
        return$this->qty;
    }
}