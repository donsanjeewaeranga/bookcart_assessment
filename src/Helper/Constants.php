<?php
namespace App\Helper;

/**
 * Class Constants
 * @package App\Helper
 */
class Constants
{
    public const IS_DEFAULT = 1;

    /**
     * Pagination
     */
    public const DEFAULT_PAGE = 1;
    public const PER_PAGE = 20;

    /**
     * Roles
     */

    public const ROLE_0 = 'ROLE_ADMIN';
    public const ROLE_1 = 'ROLE_USER';

    /**
     * Cart actions
     */

    public const ADD_TO_CART = 'add';
    public const REMOVE_FROM_CART = 'remove';

}
