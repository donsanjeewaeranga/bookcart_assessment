/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
import '../css/app.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

$(document).ready(function() {
    $('[name="bookcategory"]').change(function() {
        $(this).closest('form').submit();
    });

    // Add to cart
    var $addToCartContainer = $('button.add-to-cart');
    $addToCartContainer.on('click', function (e) {
        e.preventDefault();
        var $bookId = $(this).attr('data-book-id');
        $.fn.addToCartRequest($bookId, 1);
    });

    // Remove from cart
    // var $RemoveFromCartcontainer = $('a.remove-from-cart');
    // $RemoveFromCartcontainer.on('click', function (e) {
    //     e.preventDefault();
    //     var $bookId = $(this).attr('data-book-id');
    //     var $quantity = $(this).parent().siblings().children("input.quantity").val();
    //
    //     $.ajax({
    //         url: '/remove-from-cart/'+$bookId+'/'+$quantity,
    //         method: 'POST'
    //     }).then(function (response) {
    //         console.log(response);
    //         // $('#cart-summary-block span.item-count').text(response.items);
    //         // $('#cart-summary-block span.total-price').text(response.total);
    //     }).fail(function (error) {
    //         console.log(error);
    //     });
    // });

    var $RemoveFromCartcontainer = $('input.quantity');
    $RemoveFromCartcontainer.on('change', function (e) {
        if (this.value >= 0) {
            var direction = this.defaultValue < this.value;
            var $bookId = $(this).attr('data-book-id');
            if (direction) {
                var $qty = this.value - this.defaultValue;
                console.log($qty);
                $.fn.addToCartRequest($bookId, $qty);
            }
            else {
                console.log($qty);
                var $qty = this.defaultValue - this.value;
                $.fn.removeFromCartRequest($bookId, $qty);
            }
            location.reload();
        }
    });

    $.fn.addToCartRequest = function($id, $qty){
        $.ajax({
            url: '/add-to-cart/'+$id+'/'+$qty,
            method: 'POST'
        }).then(function (response) {
            console.log(response);
            $('#cart-summary-block span.item-count').text(response.items);
            $('#cart-summary-block span.total-price').text(response.total);
        }).fail(function (error) {
            console.log(error);
        });
    }

    $.fn.removeFromCartRequest = function($id, $qty){
        $.ajax({
            url: '/remove-from-cart/'+$id+'/'+$qty,
            method: 'POST'
        }).then(function (response) {
            console.log(response);
            $('#cart-summary-block span.item-count').text(response.items);
            $('#cart-summary-block span.total-price').text(response.total);
        }).fail(function (error) {
            console.log(error);
        });
    }

});
