<?php


namespace App\Helper;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SuccessResponse
 * @package App\Helper
 */
class SuccessResponse extends JsonResponse
{
    /**
     * Send Success response.
     * @param mixed $data
     * @return JsonResponse|null
     */
    public function __invoke($data)
    {
        return new JsonResponse(
            json_encode($data),
            Response::HTTP_OK
        );
    }
}
