<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // Creates a test normal user
         $user = new User();
         $user->setFirstName('James');
         $user->setLastName('Taylor');
         $user->setEmail('james444@gmail.com');
         $password = $this->encoder->encodePassword($user, 'abc123');
         $user->setPassword($password);
         $manager->persist($user);

         // Creates an test admin user
         $user = new User();
         $user->setFirstName('Eranga');
         $user->setLastName('Jayaweera');
         $user->setEmail('donsanjeewaeranga@gmail.com');
         $user->setRoles(['ROLE_ADMIN']);
         $password = $this->encoder->encodePassword($user, 'abc123');
         $user->setPassword($password);
         $manager->persist($user);

        $manager->flush();

        $categoryChildren = new Category();
        $categoryChildren->setName('Children');
        $manager->persist($categoryChildren);

        $categoryFiction = new Category();
        $categoryFiction->setName('Fiction');
        $manager->persist($categoryFiction);

        $manager->flush();

        $author1 = new Author();
        $author1->setName('B. James');
        $manager->persist($author1);

        $author2 = new Author();
        $author2->setName('M. Wikramasingha');
        $manager->persist($author2);
        $manager->flush();

        for ($i=0; $i<50; $i++) {
            $book = new Book();
            $book->setTitle("Children Book " . $i);
            $book->setPrice(mt_rand(1, 250)/10);
            $book->setCategory($categoryChildren);
            $book->setAuthor($author1);
            $manager->persist($book);

            $book = new Book();
            $book->setTitle("Children Book 200" . $i);
            $book->setPrice(mt_rand(1, 250)/10);
            $book->setCategory($categoryChildren);
            $book->setAuthor($author2);
            $manager->persist($book);
        }

         $manager->flush();

        for ($i=0; $i<50; $i++) {
            $book = new Book();
            $book->setTitle("Fiction Book " . $i);
            $book->setPrice(mt_rand(1, 250)/10);
            $book->setCategory($categoryFiction);
            $book->setAuthor($author1);
            $manager->persist($book);

            $book = new Book();
            $book->setTitle("Fiction Book 200" . $i);
            $book->setPrice(mt_rand(1, 250)/10);
            $book->setCategory($categoryFiction);
            $book->setAuthor($author2);
            $manager->persist($book);
        }

        $manager->flush();
    }
}
