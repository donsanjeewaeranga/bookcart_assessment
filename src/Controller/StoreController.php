<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Cart;
use App\Entity\Category;
use App\Helper\Constants;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

// Include paginator interface
use Knp\Component\Pager\PaginatorInterface;

class StoreController extends AbstractController
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="store_home")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $bookRepository = $this->getDoctrine()->getRepository(Book::class);
        $queryCategory = $request->query->get('bookcategory');
        $allBooks = $queryCategory && $queryCategory !== 'all' ? $bookRepository->findAllByCategory($queryCategory) : $bookRepository->findAll();

        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepository->findAll();

        $books = $paginator->paginate(
            $allBooks,
            $request->query->getInt('page', 1),
            Constants::PER_PAGE
        );

        return $this->render('store/home.html.twig', [
            'books' => $books,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/cart", name="store_cart")
     */
    public function cart()
    {
        $cart = $this->session->has('cart') ? $this->session->get('cart') : null;

        return $this->render( 'cart/cart.html.twig', [
            'cart' => $cart
        ]);
    }

    /**
     * @Route("/checkout", name="store_checkout")
     */
    public function checkout()
    {
        $cart = $this->session->has('cart') ? $this->session->get('cart') : null;

        return $this->render( 'cart/checkout.html.twig', [
            'cart' => $cart
        ]);
    }

    /**
     *
     * @Route("/add-to-cart/{id}/{qty}", name="add_to_cart")
     */
    public function addToCart($id, $qty)
    {
        $cart = $this->manageCartUpdate($id, Constants::ADD_TO_CART, $qty);

        return $this->json([
            'items' => $cart->getTotalQuantity(),
            'total' => $cart->getDiscountPrice()
        ]);
    }

    /**
     *
     * @Route("/remove-from-cart/{id}/{qty}", name="remove_from_cart")
     */
    public function removeFromCart($id, $qty)
    {
        if (!$this->session->has('cart')) {
            return $this->json([
                'items' => 0,
                'total' => 0
            ]);
        }
        $cart = $this->manageCartUpdate($id, Constants::REMOVE_FROM_CART, $qty);

        return $this->json([
            'items' => $cart->getTotalQuantity(),
            'total' => $cart->getDiscountPrice()
        ]);
    }

    private function manageCartUpdate($id, $action, $qty)
    {
        $bookRepository = $this->getDoctrine()->getRepository(Book::class);
        $book = $bookRepository->find($id);
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $cookCategory = $categoryRepository->findOneBy(['id' => $book->getCategory()]);

        $oldCart = $this->session->has('cart') ? $this->session->get('cart') : null;
        $cart = new Cart($oldCart);

        if ($action == Constants::ADD_TO_CART) {
            $cart->add($book, $cookCategory, $qty);
        } else {
            $cart->remove($book, $cookCategory, $qty);
        }

        if ($cart->getTotalQuantity() == 0) {
            $cart = null;
        }

        $this->session->set('cart', $cart);
        return $cart;
    }

}
