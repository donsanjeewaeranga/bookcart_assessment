<?php

namespace App\Entity;


class Cart
{
    private $items = null;
    private $childrenBooks = null;
    private $fictionBooks = null;
    private $totalQty = 0;
    private $discount = 0;
    private $totalPrice = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;

            if ($oldCart->childrenBooks) {
                $this->childrenBooks = $oldCart->childrenBooks;
            }

            if ($oldCart->fictionBooks) {
                $this->fictionBooks = $oldCart->fictionBooks;
            }

            $this->discount = $oldCart->discount;
        }
    }

    /**
     * @param $item
     * @param $category
     * @param $qty
     */
    public function add($item, $category, $qty)
    {
        $id = $item->getId();
        $storeItem = ['qty' => 0, 'price' => $item->getPrice(), 'item' => $item];

        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storeItem = $this->items[$id];
            }
        }

        if ($category->getName() == 'Children') {
            $oldChildrenBooks = $this->childrenBooks ? $this->childrenBooks : null;
            $this->childrenBooks = new CartChildrenBook($oldChildrenBooks);
            $this->childrenBooks->add($item, $qty);
        }

        if ($category->getName() == 'Fiction') {
            $oldFictionBooks = $this->fictionBooks ? $this->fictionBooks : null;
            $this->fictionBooks = new CartFictionBook($oldFictionBooks);
            $this->fictionBooks->add($item, $qty);
        }

        $storeItem['qty'] += $qty;
        $storeItem['price'] = $item->getPrice() * $storeItem['qty'];
        $this->items[$id] = $storeItem;
        $this->totalQty += $qty;

        $this->discount = $this->childrenBooks->getDiscount() + $this->calculateCategoryDiscount();
        $this->totalPrice += ($item->getPrice() * $qty);
    }

    /**
     * @param $item
     * @param $category
     * @param $qty
     * @return bool
     */
    public function remove($item, $category, $qty)
    {
        $id = $item->getId();

        if (!$this->items || !array_key_exists($id, $this->items)) {
            return false;
        }

        $storeItem = $this->items[$id];

        if ($category->getName() == 'Children') {
            $oldChildrenBooks = $this->childrenBooks ? $this->childrenBooks : null;
            $this->childrenBooks = new CartChildrenBook($oldChildrenBooks);
            $this->childrenBooks->remove($item, $qty);
        }

        if ($category->getName() == 'Fiction') {
            $oldFictionBooks = $this->fictionBooks ? $this->fictionBooks : null;
            $this->fictionBooks = new CartFictionBook($oldFictionBooks);
            $this->fictionBooks->remove($item, $qty);
        }

        $storeItem['qty'] -= $qty;

        if ($storeItem['qty'] == 0) {
            unset($this->items[$id]);
        } else {
            $storeItem['price'] = $item->getPrice() * $storeItem['qty'];
            $this->items[$id] = $storeItem;
        }
        $this->totalQty -= $qty;
        $this->discount = $this->childrenBooks->getDiscount() + $this->calculateCategoryDiscount();
        $this->totalPrice -= ($item->getPrice() * $qty);
    }

    /**
     * @return int
     */
    public function getTotalQuantity()
    {
        return $this->totalQty;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return round($this->totalPrice, 2);
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @return mixed
     */
    public function getChildrenDiscount()
    {
        return $this->childrenBooks->getDiscount();
    }

    public function getCategoryDiscount()
    {
        return $this->calculateCategoryDiscount();
    }

    /**
     * @return int
     */
    public function getDiscountPrice()
    {
        return round($this->totalPrice - $this->discount, 2);
    }

    /**
     * @return null
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return float|int
     */
    private function calculateCategoryDiscount()
    {
        if (!$this->isMoreThan10Each()) {
            return  0;
        }

        return $this->totalPrice * 0.05;
    }

    /**
     * @return bool
     */
    private function isMoreThan10Each()
    {
        if (!$this->childrenBooks || !$this->fictionBooks) {
            return false;
        }

        if ($this->childrenBooks->getQuantity() < 10 || $this->fictionBooks->getQuantity() < 10) {
            return false;
        }

        return true;
    }
}